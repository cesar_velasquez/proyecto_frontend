<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Maquetaci&oacute;n Web</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
        <link rel="stylesheet" type="text/css" href="assets/css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css" media="screen" />
        
    </head>
    <body>
    <center>
        <header class="col-md-12 col-sm-12 col-xs-12">
            <div id="header_top" class="row">
                <div id="header_top_logo" class="col-md-6 col-xs-12">
                    <img src="img/logo.png" />
                </div>
                <div id="header_top_menu" class="col-md-6 col-xs-12">
                    <nav class="navbar">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-navegacion-collapse">
                                    <span class="sr-only">Cambiar Navegaci&oacute;n</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="navbar-navegacion-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="item_menu"><a href="index.html">HOME</a></li>
                                    <li class="item_menu"><a href="#">SOBRE</a></li>
                                    <li class="item_menu"><a href="#">SERVI&Ccedil;OS</a></li>
                                    <li class="item_menu"><a href="#">PORTAFOLIO</a></li>
                                    <li class="item_menu"><a href="form.php">CONTATO</a></li>
                                </ul>
                                <div id="boton_buscar"><img src="img/boton_buscar.png" /></div>
                            </div>
                        </div>
                    </nav>
                    <div id="twitter">
                        <p><span>@FernandynhoMC</span> This was the most rewarding educational experience that I have ever had”</p>
                    </div>
                </div>
            </div>
            <div id="header_botton" class="row">
                <div id="body_slider" class="col-md-12 col-xs-12">
                    
                    <div id="carousel-servicios" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-servicios" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-servicios" data-slide-to="1"></li>
                            <li data-target="#carousel-servicios" data-slide-to="2"></li>
                            <li data-target="#carousel-servicios" data-slide-to="3"></li>
                            <li data-target="#carousel-servicios" data-slide-to="4"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            
                            <div id="images_slider" class="item active">
                                <img class="img-responsive" id="imgr0" src="img/slider.jpg">
                            </div>
                            <div id="images_slider" class="item">
                                <img class="img-responsive" id="imgr1" src="img/slider.jpg">
                            </div>
                            <div id="images_slider" class="item">
                                <img class="img-responsive" id="imgr2" src="img/slider.jpg">
                            </div>
                            <div id="images_slider" class="item">
                                <img class="img-responsive" id="imgr3" src="img/slider.jpg">
                            </div>
                            <div id="images_slider" class="item">
                                <img class="img-responsive" id="imgr4" src="img/slider.jpg">
                            </div>
                            
                        </div>
                    </div>
                    <div id="botones_slider"></div>
                </div>
            </div>
        </header>
    </center>
        <div id="body" class="col-md-12 col-sm-12 col-xs-12" style="margin: 10px 0 0 0;">
            <div class="panel panel-primary">
                <div class="panel-heading">Formulario de Contacto</div>
                <div class="panel-body" style="margin: 10px 0 0 0;">
                    <form method="post" id="formid">
                        <!--<div id="ok"></div>-->
                        <div class="row">
                            <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Nombres">
                        </div>
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Apellidos">
                        </div>
                        <div class="form-group">
                            <label for="genero">G&eacute;nero</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radiogenero" id="optionsRadios1" value="Masculino">
                                    Masculino
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radiogenero" id="optionsRadios2" value="Femenino">
                                    Femenino
                                </label>
                            </div>
                            <label for="radiogenero" class="error"></label>
                        </div>
                        <div class="form-group">
                            <label for="telefono">Tel&eacute;fono</label>
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Teléfono">
                        </div>
                        <div class="form-group">
                            <label for="edad">Edad</label>
                            <input type="text" class="form-control" name="years" id="years" placeholder="Edad">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="clave">Clave</label>
                            <input type="password" class="form-control" name="clave" id="clave" placeholder="Clave">
                        </div>
                        <div class="form-group">
                            <label for="deporte">Deporte</label>
                            <select class="form-control" name="deporte" id="deporte">
                                <option value="">Seleccione Deporte</option>
                                <option value="Tenis">Tenis</option>
                                <option value="Basquet">Basquet</option>
                                <option value="Futbol">Futbol</option>
                                <option value="Natación">Natación</option>
                                <option value="Ciclismo">Ciclismo</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="mensaje">Mensaje</label>
                            <textarea class="form-control" id="message" name="message" placeholder="Mensaje" rows="5" cols="53"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="file">Subir Archivo</label>
                            <input type="file" name="archivo" id="archivo">
                            <!--<p class="help-block">Example block-level help text here.</p>-->
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="politicas" name="politicas" />Pol&iacute;ticas de privacidas
                            </label>
                            <label for="politicas" class="error"></label>
                        </div>
                        <button type="submit"class="btn btn-default" value="Enviar">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <center>
        <footer class="col-md-12 col-sm-12 col-xs-12">
            <div id="footer" class="row">
                <div id="footer_links">
                    <div id="links" class="col-md-8 col-xs-12">
                        <div class="lista_links">
                            <span>Useful Links</span>
                            <ul>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                            </ul>
                        </div>
                        <div class="lista_links">
                            <div><span>Useful Links</span></div>
                            <ul>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                            </ul>
                        </div>
                        <div class="lista_links">
                            <div><span>Useful Links</span></div>
                            <ul>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                            </ul>
                        </div>
                        <div class="lista_links">
                            <div><span>Useful Links</span></div>
                            <ul>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                                <li><a href="">This is a link</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="footer_logo" class="col-md-4 col-xs-12">
                        <img src="img/logo.png" />
                    </div>
                </div>
                <div id="footer_logo_social">
                    <div id="footer_copy" class="col-md-9 col-xs-12">&copy; Copyright 2010 Locus Onepage. All Rights Reserved.</div>
                    <div id="footer_social" class="col-md-3 col-xs-12">
                            <ul>
                                <li class="logo_social"><a href=""><img src="img/logo_face.png" /></a></li>
                                <li class="logo_social"><a href=""><img src="img/logo_social_uno.png" /></a></li>
                                <li class="logo_social"><a href=""><img src="img/logo_social_dos.png" /></a></li>
                                <li class="logo_social"><a href=""><img src="img/logo_social_tres.png" /></a></li>
                                <li class="logo_social"><a href=""><img src="img/logo_social_cuatro.png" /></a></li>
                                <li class="logo_social"><a href=""><img src="img/logo_social_cinco.png" /></a></li>
                            </ul>
                    </div>
                </div>
            </div>
        </footer>
    </center>
    </body>
</html>
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    /*$("#ok").hide();*/
    
    $.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg != value;
    }, "El valor no debe ser igual a arg (vacio)");
    
    $("#formid").validate({
        rules: {
            name: { required: true, minlength: 3, maxlength : 20},
            lastname: { required: true, minlength: 2},
            email: { required: true, email: true},
            clave: { required: true, minlength: 6},
            phone: { required: true, minlength: 5, maxlength: 15, number: true},
            years: { required: true, number: true, maxlength : 2},
            message: { required: true, minlength: 2},
            deporte: { /*required: true,*/ valueNotEquals: ""},
            politicas: {required: true},
            archivo: {
                required: true,
                extension: "jpg|png|gif"
            },
            radiogenero: { required: true}

        },
        messages: {
            name : {
                required : "Debe ingresar su nombre.",
                minlength : "El nombre debe tener un minimo de 3 caracteres.",
                maxlength : "El nombre debe tener un maximo de 20 caracteres."
            },
            lastname: "Debe ingresar su apellido.",
            email : {
                required : "Debe ingresar un email.",
                email    : "Debe ingresar un email valido."
            },
            clave : {
                required : "Debe ingresar una clave.",
                minlength : "La clave debe tener un minimo de 6 caracteres."
            },
            phone : "El número de teléfono no es correcto.",
            years : {
                required : "Ingrese su edad.",
                number : "Debe ingresar solo dígitos.",
                maxlength : "Máximo dos digitos."
            },
            message : "El campo mensaje es obligatorio.",
            deporte: {
                valueNotEquals: "Seleccione un deporte.",
                required: "Seleccione deporte."
            },
            politicas: {
                required : "Acepte las políticas."
            },
            archivo: {
                required: "Seleccione una imagen",
                extension: "Archivo incorrecto, se acepta png, jpg o gif"
            },
            radiogenero: {
                required: "Seleccione un género"
            }
        }/*,
        submitHandler: function(form){
            var dataString = 'name='+$('#name').val()+'&lastname='+$('#lastname').val()+'...';
            $.ajax({
                type: "POST",
                url:"send.php",
                data: dataString,
                success: function(data){
                    $("#ok").html(data);
                    $("#ok").show();
                    $("#formid").hide();
                }
            });
        }*/
    });
});
</script>